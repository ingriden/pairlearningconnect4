def mergeInterval(intervals, newInterval):
    solution = []
    temporary = None
    
    for pos in range(len(intervals)):
        curr = intervals[pos]

        # at beginning of list
        if curr[1] < newInterval[0]:
            solution.append(curr)
            if pos == len(intervals) - 1:
                solution.append(newInterval)
        
        # at end of list
        elif curr[0] > newInterval[1]:
            if (pos == 0 or 
                len(solution) and solution[-1][1] < newInterval[0]
            ):
                solution.append(newInterval)
            solution.append(curr)
            
        else:
            if temporary:
                minVal = min(curr[0], temporary[0])
                maxVal = max(curr[1], temporary[1])
                solution[-1] = [minVal, maxVal]
            else:
                minVal = min(curr[0], newInterval[0])
                maxVal = max(curr[1], newInterval[1])
                solution.append([minVal, maxVal])
            temporary = solution[-1]
    
    return solution

A = [[-1,0],  [1,3],[4,5],[6,9],  [10,11]]
B = [2,8]
print(mergeInterval(A, B))

B = [-5,-4]
print(mergeInterval(A, B)) # add at start

B = [12,15]
print(mergeInterval(A, B)) # add at end

B = [-2,12]
print(mergeInterval(A, B)) #[-2,112]

A = [[-1,0], [10,11]]
B = [2,8]
print(mergeInterval(A, B))

            

'''
new interval: [4,8]

solution = [-1,0]

index: 1
[1,3] and [2,8]
temporary: [1,8]

index: 2
temporary: [1,8]
curr: [4,5]
nextInterval: [1,8]

index: 3
curr: [6,9]
nextInterval: [1,9]

index: 4
curr: [10,11]

'''